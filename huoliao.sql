-- MySQL dump 10.13  Distrib 8.0.12, for macos10.13 (x86_64)
--
-- Host: localhost    Database: huoliao
-- ------------------------------------------------------
-- Server version	8.0.12

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cdc_comment`
--

DROP TABLE IF EXISTS `cdc_comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cdc_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '吐槽评论',
  `user_id` varchar(45) DEFAULT NULL COMMENT '用户id',
  `information_id` varchar(45) DEFAULT NULL COMMENT '信息id',
  `content` varchar(225) DEFAULT NULL COMMENT '内容',
  `status` tinyint(1) DEFAULT NULL,
  `create_time` bigint(13) DEFAULT NULL,
  `update_time` bigint(13) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cdc_comment`
--

LOCK TABLES `cdc_comment` WRITE;
/*!40000 ALTER TABLE `cdc_comment` DISABLE KEYS */;
INSERT INTO `cdc_comment` VALUES (1,'1','1','评论1',1,1528872754218,1528872754218),(2,'1','2','吐槽1',1,1528872755360,1528872820957),(4,'1','4','吐槽',1,1528872756677,1528872756677),(5,'1','1','吐槽',1,1528872757416,1528872757416),(7,'1','1','手机吐槽',1,1528874999503,1528874999503),(8,'1','1','手机吐槽',1,1528875057088,1528875057088),(9,'1','1','手机吐槽',1,1528875067694,1528875067694);
/*!40000 ALTER TABLE `cdc_comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cdc_friend`
--

DROP TABLE IF EXISTS `cdc_friend`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cdc_friend` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '0  1添加2; 1 好友',
  `user_id1` varchar(45) DEFAULT NULL,
  `user_id2` varchar(45) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `create_time` bigint(13) DEFAULT NULL,
  `update_time` bigint(13) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cdc_friend`
--

LOCK TABLES `cdc_friend` WRITE;
/*!40000 ALTER TABLE `cdc_friend` DISABLE KEYS */;
INSERT INTO `cdc_friend` VALUES (4,'4','1',1,1529572602420,1529572602420);
/*!40000 ALTER TABLE `cdc_friend` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cdc_group`
--

DROP TABLE IF EXISTS `cdc_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cdc_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupName` varchar(45) DEFAULT NULL COMMENT '群组名',
  `status` tinyint(1) DEFAULT NULL,
  `create_time` bigint(13) DEFAULT NULL,
  `update_time` bigint(13) DEFAULT NULL,
  `userId` varchar(45) DEFAULT NULL COMMENT '创建userid',
  `avatar` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cdc_group`
--

LOCK TABLES `cdc_group` WRITE;
/*!40000 ALTER TABLE `cdc_group` DISABLE KEYS */;
INSERT INTO `cdc_group` VALUES (6,'rere',1,1529907080087,1532588167561,'1','http://7xogjk.com1.z0.glb.clouddn.com/IuDkFprSQ1493563384017406982'),(7,'群组1',1,1529907139883,1529907139883,'1',NULL),(8,'群组1',1,1529907181413,1529907181413,'1',NULL),(9,'群组1',1,1533191480246,1533191480246,'1',NULL);
/*!40000 ALTER TABLE `cdc_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cdc_group_user`
--

DROP TABLE IF EXISTS `cdc_group_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cdc_group_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` varchar(45) DEFAULT NULL,
  `user_id` varchar(45) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `create_time` bigint(13) DEFAULT NULL,
  `update_time` bigint(13) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cdc_group_user`
--

LOCK TABLES `cdc_group_user` WRITE;
/*!40000 ALTER TABLE `cdc_group_user` DISABLE KEYS */;
INSERT INTO `cdc_group_user` VALUES (5,'6','1',1,1529907080091,1529907080091),(6,'7','1',1,1529907139889,1529907139889),(7,'8','1',1,1529907181421,1529907181421),(8,'9','1',1,1533191480271,1533191480271);
/*!40000 ALTER TABLE `cdc_group_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cdc_information`
--

DROP TABLE IF EXISTS `cdc_information`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cdc_information` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '吐槽内容',
  `title` varchar(45) DEFAULT NULL COMMENT '标题',
  `content` text COMMENT '内容（富文本）',
  `status` tinyint(1) DEFAULT NULL COMMENT '状态 0 下线 1 上线',
  `create_time` bigint(13) DEFAULT NULL COMMENT '创建时间',
  `update_time` bigint(13) DEFAULT NULL COMMENT '更新时间',
  `bullish` int(11) DEFAULT '0' COMMENT '看好',
  `bearish` int(11) DEFAULT '0' COMMENT '看空',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cdc_information`
--

LOCK TABLES `cdc_information` WRITE;
/*!40000 ALTER TABLE `cdc_information` DISABLE KEYS */;
INSERT INTO `cdc_information` VALUES (1,'test','<p>123</p>',1,1528871575727,1531992599893,2,3),(2,'test','<p>1234</p>',1,1528871584242,1528871670264,5,1),(4,'test','<p>123</p>',1,1528871586059,1528871586059,5,1),(5,'标题','内容内容',1,1529631102010,1529631102010,1,1);
/*!40000 ALTER TABLE `cdc_information` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cdc_member`
--

DROP TABLE IF EXISTS `cdc_member`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cdc_member` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '后台管理员',
  `name` varchar(45) DEFAULT NULL COMMENT '用户名',
  `password` varchar(225) DEFAULT NULL COMMENT '密码',
  `status` tinyint(1) DEFAULT NULL COMMENT '状态 0 不可用 1 可用',
  `role_id` varchar(45) DEFAULT NULL COMMENT '角色（备用）',
  `last_login_ip` varchar(45) DEFAULT NULL COMMENT '上次登录ip（备用）',
  `last_login_time` bigint(13) DEFAULT NULL COMMENT '上次登录时间（备用）',
  `create_time` bigint(13) DEFAULT NULL COMMENT '创建时间',
  `update_time` bigint(13) DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cdc_member`
--

LOCK TABLES `cdc_member` WRITE;
/*!40000 ALTER TABLE `cdc_member` DISABLE KEYS */;
INSERT INTO `cdc_member` VALUES (1,'test','$2a$10$M7LJS6gwo4q2UwsdyviW7eT/.LZL6wgtKuVeMr330kRAqYIOcsPeW',1,'0','5',1,1528783605799,1528783605799),(2,'test','$2a$10$MQbfXi0GK9b5JRAUSKTQJO16JwnVGY7dT/GxlciVfbV2RZ7qmjHrm',1,'0','5',1,1528783608260,1528783648250);
/*!40000 ALTER TABLE `cdc_member` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cdc_mobilemessage`
--

DROP TABLE IF EXISTS `cdc_mobilemessage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cdc_mobilemessage` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '手机短信，后期删除该表',
  `code` varchar(45) DEFAULT NULL,
  `mobile` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cdc_mobilemessage`
--

LOCK TABLES `cdc_mobilemessage` WRITE;
/*!40000 ALTER TABLE `cdc_mobilemessage` DISABLE KEYS */;
INSERT INTO `cdc_mobilemessage` VALUES (14,'159004','18661772011'),(15,'118848','123456'),(16,'127529','1234567');
/*!40000 ALTER TABLE `cdc_mobilemessage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cdc_order`
--

DROP TABLE IF EXISTS `cdc_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cdc_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '每人抢到的红包订单',
  `redenvelope_id` int(11) DEFAULT NULL COMMENT '红包id',
  `money` varchar(45) DEFAULT NULL COMMENT '获得的金额',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `status` tinyint(1) DEFAULT NULL,
  `create_time` bigint(13) DEFAULT NULL,
  `update_time` bigint(13) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cdc_order`
--

LOCK TABLES `cdc_order` WRITE;
/*!40000 ALTER TABLE `cdc_order` DISABLE KEYS */;
INSERT INTO `cdc_order` VALUES (1,1,'123',1,0,1545029714866,1545029833609),(2,1,'123',1,0,1545029763673,1545029763673),(3,5,'1',1,1,1545105465612,1545105465612),(4,5,'1',1,1,1545105518029,1545105518029),(5,9,'75',1,1,1545355211083,1545355211083),(6,9,'12',1,1,1545355234048,1545355234048),(7,9,'10',1,1,1545355244007,1545355244007),(8,9,'1',1,1,1545355249376,1545355249376),(9,9,'1',1,1,1545355253780,1545355253780),(10,9,'2',1,1,1545355389138,1545355389138),(11,9,'6',1,1,1545355395792,1545355395792),(12,9,'12',1,1,1545355403822,1545355403823);
/*!40000 ALTER TABLE `cdc_order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cdc_packet`
--

DROP TABLE IF EXISTS `cdc_packet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cdc_packet` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '红包',
  `user_id` varchar(45) DEFAULT NULL COMMENT '用户id',
  `total_num` int(11) DEFAULT NULL COMMENT '糖果总数',
  `left_num` int(11) DEFAULT NULL COMMENT '糖果剩余',
  `total_times` int(11) DEFAULT NULL COMMENT '总次数',
  `left_times` int(11) DEFAULT NULL COMMENT '剩余次数',
  `create_time` bigint(13) DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cdc_packet`
--

LOCK TABLES `cdc_packet` WRITE;
/*!40000 ALTER TABLE `cdc_packet` DISABLE KEYS */;
INSERT INTO `cdc_packet` VALUES (1,NULL,NULL,NULL,NULL,NULL,1530671592930);
/*!40000 ALTER TABLE `cdc_packet` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cdc_redenvelope`
--

DROP TABLE IF EXISTS `cdc_redenvelope`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cdc_redenvelope` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '红包',
  `user_id` int(11) DEFAULT NULL COMMENT '红包发起者',
  `type` tinyint(1) DEFAULT NULL COMMENT '红包类型 1个人红包2群里红包',
  `money` int(11) DEFAULT NULL COMMENT '金额总量',
  `number` int(11) DEFAULT NULL COMMENT '数量',
  `left_money` int(11) DEFAULT NULL COMMENT '剩余金额',
  `left_number` int(11) DEFAULT NULL COMMENT '红包剩余数量',
  `group_id` int(11) DEFAULT NULL COMMENT '群组id，type为1时默认0',
  `token` varchar(45) DEFAULT NULL COMMENT '货币类型',
  `title` text COMMENT '留言',
  `status` tinyint(1) DEFAULT NULL COMMENT '状态',
  `create_time` bigint(13) DEFAULT NULL,
  `update_time` bigint(13) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cdc_redenvelope`
--

LOCK TABLES `cdc_redenvelope` WRITE;
/*!40000 ALTER TABLE `cdc_redenvelope` DISABLE KEYS */;
INSERT INTO `cdc_redenvelope` VALUES (9,1,1,100,5,0,0,1,'btc','ceshi',1,1545354211230,1545354211230);
/*!40000 ALTER TABLE `cdc_redenvelope` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cdc_signin`
--

DROP TABLE IF EXISTS `cdc_signin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cdc_signin` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '签到',
  `user_id` varchar(45) DEFAULT NULL COMMENT '用户id',
  `create_time` bigint(13) DEFAULT NULL COMMENT '创建时间',
  `points` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cdc_signin`
--

LOCK TABLES `cdc_signin` WRITE;
/*!40000 ALTER TABLE `cdc_signin` DISABLE KEYS */;
INSERT INTO `cdc_signin` VALUES (1,'1',1528783958433,1),(2,'1',1528783958433,1),(3,'1',1528946880710,3),(4,'1',1528946904727,3);
/*!40000 ALTER TABLE `cdc_signin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cdc_user`
--

DROP TABLE IF EXISTS `cdc_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cdc_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mobile` varchar(45) DEFAULT NULL COMMENT '用户手机号',
  `name` varchar(45) DEFAULT NULL COMMENT '用户名',
  `email` varchar(45) DEFAULT NULL COMMENT '用户email',
  `portrait` text,
  `bonus_points` int(11) DEFAULT NULL COMMENT '普通积分（备用）',
  `sugar_points` int(11) DEFAULT NULL COMMENT '红包（糖果）',
  `vip_level` tinyint(1) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `invitationCode` int(8) DEFAULT NULL,
  `from_user_id` varchar(45) DEFAULT NULL,
  `create_time` bigint(13) DEFAULT NULL,
  `update_time` bigint(13) DEFAULT NULL,
  `rongyun_token` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cdc_user`
--

LOCK TABLES `cdc_user` WRITE;
/*!40000 ALTER TABLE `cdc_user` DISABLE KEYS */;
INSERT INTO `cdc_user` VALUES (1,'212121','222','22','22',22,148,1,1,11,NULL,NULL,1532588383177,NULL);
/*!40000 ALTER TABLE `cdc_user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-12-21  9:55:38
